Introduccion

El Algebra Secuencial es un campo de las Matematicas, operaciones de datos extensas.

En terminos practicos, tiene el mismo proposito que el ALgebra Relacional, y es una alternativa a la misma, corrigiendo algunas discrepancias, y provee algunas actualizaciones,, con el fin de ayudar al aprendizaje los Desarrolladores de Software.

Secuencia: Es una coleccion de una dimension, compuesta por elementos que son del mismo tipo de datos.

Los elementos de una secuencia, se ordenan como Primer elemento que llega, primer Elemento que Sale.

Y a diferencia de el Algebra Relaional, se permiten elementos duplicados.

Nota: En ALgebra Secuencial, las secuencias son similares, pero no iguales, tanto a los conjuntos de la Teoria de Conjuntos, como a los vectores en Algebra Lineal.

Pero, puede haber mas de una instancia para cada elemento, y pueden ordenarse. Tambien son similares, a los a los vectores de Algebra Lineal. Pero, elementos individuales no pueden ser accesados, como lo son los elementos de un vector.
