Overview 

Sequential Algebra is a Mathematics field, designed for large data operations.

In Practical terms, it has the same purpose as Relational Algebra, and it is an alternative to it, by correcting some issues, and by providing some updates, in order to help Software Programmers� learning. 

Sequence: Single dimension collection, composed by items with the same data type.

The elements in a sequence, are sorted as First item to get In, First item to get out. 

And, unlike Relational Algebra, duplicated items are allowed. 

Note: In Sequential Algebra, sequences are similar, but not equal, to both Set Theory�s sets and Linear Algebra vectors.

But, there can be more than one instance for the same item, and can be also sorted. They are also similar, to Lineal Algebra�s vectors. But, individual items can not be accessed, as vector�s items does. 
